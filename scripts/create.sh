#!/bin/bash

# This script is made to create new views, it needs 1 parameter, the view name
# It needs to be executed from the root of the project : $ ./scripts/create.sh <ComponentName>
# Once executed, it checks whether a view name has been provided or not. If not, returns an error
# If the view as correctly been provided, it checks whether the view already exists (within the Views and the Components lists)
# If it exists, it returns an error, if not it creates and declare the view, and it creates the Component folder
# At the end, a javascript prompt is displayed to import the new view in the router component

clear
echo 'Creation of a new component'
echo

if [ $# -eq 1 ]
then
	c=$1

	declare -l component
	component=$c

	declare -c Component
	Component=$c

	#Create component folder
	componentFolder=src/tsx/Components/$Component
	if test -e "$componentFolder"; then
		echo "$componentFolder components Folder already exists."
		echo
		echo 'Done with error'
		exit
	fi
	mkdir $componentFolder

	#create view
	view=src/tsx/Views/$Component.tsx
	if test -f "$view"; then
		echo "$view View already exists."
		echo
		echo 'Done with error'
		exit
	fi
	touch $view
	echo 'import React from "react";' >> $view
	echo '' >> $view
	echo "const $Component = () => (<></>)" >> $view
	echo '' >> $view
	echo "export default $Component;" >> $view
	
	echo 'Done φ(*⌒▽⌒)ﾉ'
	echo
	echo 'You can now import your new View in AppRouter.tsx and create a new route for it as following :'
	echo "import $Component from './tsx/Views/$Component.tsx';"
	echo
else
	echo "Please provide a component name"
	echo
	echo 'Done with error'

fi
