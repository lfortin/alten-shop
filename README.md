# React + TypeScript + Vite

This project was developed using Vite and TypeScript as part of a job interview for Alten.

It is a fork of the "react-seed" project, an open-source React TypeScript project developed by Louis Fortin.

## Installation

To install the project, ensure you have Node.js version 18 or higher installed. You can use nvm to switch to Node.js version 20 by running nvm use.

```bash
yarn install
```

## Running the Project

You can launch the project on port 4200 (defined in the .env file) using the following command:

```bash
yarn dev
```

## Linting

To check the linter, run:

```bash
yarn lint
```

## Security Audit

To check for vulnerabilities, run:

```bash
yarn audit
```

## Integration Tests

Integration tests have been written using Playwright. You can run all tests with:

```bash
yarn p-test
```

To run tests from a specific folder (stored /playwright-tests/features/ in e.g., /playwright-tests/features/folder_name), use:

```bash
yarn p-test:folder folder_name
```

## Rendering Efficiency Inspection

To inspect rendering efficiency, you can use the React Developer Tools add-on:

- [Chrome](https://chromewebstore.google.com/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
- [Firefox](https://addons.mozilla.org/fr/firefox/addon/react-devtools/)

To activate the rendering highlights:

- Inspect the web page (right click -> inspect or F12)
- Go to the Components tab of your inspector
- In the settings check the "Highlight updates when components render." option
