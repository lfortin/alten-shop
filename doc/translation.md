# Language Management Documentation

### Introduction

The language management in the application is facilitated through the i18n library: [react-intl](https://www.npmjs.com/package/react-intl). This documentation provides details on the structure and processes involved in managing languages.

### i18n Library Overview

The i18n library comprises multiple JSON files containing translation keys for different languages, such as en.json for English and fr.json for French. It also includes an [initReactIntl.ts](/src/libs/i18n/initReactIntl.ts) library responsible for managing translations based on the targeted language. This file generates the cache for the react-intl library and creates the IntlShape for the application using the createIntl function. Additionally, it declares possible values for the locale:

```typescript
export const EN: string = "EN";
export const FR: string = "FR";

export type LocaleType = typeof EN | typeof FR;
export const defaultLocale = EN;
```

### AppRouter Integration

The [AppRouter.tsx](/src/AppRouter.tsx) files injects the intl library into the application. It retrieves the list of messages from the initReactIntl library and the locale from the useLocaleStateStore store.

This file does not need to be edited and serves as a central integration point for language management.

### LocaleSelector Component

A LocaleSelector component is provided in [LocaleSelector.tsx](/src/tsx/Components/LocaleSelector/LocaleSelector.tsx) for changing the language within the application. The rendering of this component can be customized, and the changeLocale function handles the language change process. It performs the following steps:

- Records the selected locale value in the useLocaleStateStore store (cf [store.md](store.md)). This allows components needing knowledge of the application's language to subscribe to this store.
- Calls the switchLocale function from the initReactIntl library, changing the list of messages provided to intl based on the chosen language.

### Translation Methods

There are several ways to translate text in the application:

- Import useIntl from the react-intl library in a React component and use the formatMessage function:

```typescript
import { useIntl } from "react-intl";
...
const intl = useIntl();
const msg = intl.formatMessage({ id: 'translation.key' });
```

This method is suitable for React components.

- Import intl directly from the initReactIntl library and use the formatMessage function:

```typescript
import { intl } from "/src/i18n/initReactIntl";
...
const msg = intl.formatMessage({ id: 'translation.key' });
```

This method can be used in any file.

- Use FormattedMessage from the react-intl library in a React component:

```typescript
import { FormattedMessage } from "react-intl";
...
return (<div><FormattedMessage id="translation.key" /></div>);
```

This method is suitable for React components.

### Adding Translation Keys

To add a translation key, declare it in all JSON files in the i18n folder, like this:

```json
"translation.key": "my message"
```

For complex examples involving variables or handling numbers, including potential plurals, refer to keys like "home.title" (homepage, /home) and "tabbed.message.from_parent" (Tab view, /tab-alpha & /tab-beta).

### Adding new language Keys

To add a new language, follow these steps :

- create a new language JSON file into the i18n folder (lg.json in this example)
- Add le language key (LG in this example) to the declared languages in the initReactintl library, and add type to the possible LocaleType

```typescript
import lg from "./lg.json";
export const LG: string = "LG";
```

- Add it to the switchLocale logic : create a new switch case. If you want this language to be your new default language, just set the defaultLocale constant to your new language key

```typescript
case LG:
	intl.messages = lg;
	break;
```

- Import the new locale in the LocaleSelector and add it to the available options

```typescript
import {
  /* existing locales */
  LG,
  LocaleType,
  defaultLocale,
  switchLocale,
} from "../../../libs/i18n/initReactIntl";

const localeOptions: DropdownOption[] = [
  // existing options
  { value: LG, label: frLabel },
];
```
