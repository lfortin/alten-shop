# Fonts Management Documentation

This documentation provides insights into how fonts are managed in the application, utilizing [Google Fonts](https://fonts.google.com/). The fonts are imported and declared in the [fonts.css](/src/css/fonts.css).

### Google Fonts Import

The fonts are imported in the fonts.css file using the following CSS:

```css
/* Roboto - Regular 400 */
@import url("https://fonts.googleapis.com/css2?family=Roboto:ital@0;1&display=swap");

.roboto-regular {
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  font-style: normal;
}

.roboto-regular-italic {
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  font-style: italic;
}
```

### Global Accessibility

The fonts.css file is imported in the [index.css](/src/index.css) file. This ensures that all fonts declared in the fonts.css file are accessible throughout the entire application.

```css
@import url(./css/fonts.css);

body {
  margin: 0;
  font-family: "Roboto", sans-serif; /* default font */
  font-weight: 400;
  font-style: normal;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
```

The default font for the entire application is set to "roboto-regular". The italicized version of the font is accessible using the CSS class "roboto-regular-italic".

```html
<div class="roboto-regular">Regular Text</div>
<div class="roboto-regular-italic">Italicized Text</div>
```

Feel free to utilize these classes in your application to achieve consistent and aesthetically pleasing typography.
