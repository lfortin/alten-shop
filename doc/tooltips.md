# Tooltips Usage Documentation

This documentation provides guidelines for using tooltips in the application. The decision to move away from the `react-tooltip` library was made due to issues with its behavior and the need for a more flexible and performant solution.

## Tooltip Implementation

### 1. Tooltip Hook

The application utilizes a custom hook [tooltips.ts](/src/libs/utils/tooltips.ts). This hook reacts to the hover events of HTML elements, calculating dimensions and theoretical absolute positions of tooltips. The hook saves this information in the Redux store. Additionally, it can trigger a Redux action during hover to update the data and change the `visible` attribute to `true` (and `false` on `onLeave`).

### 2. Tooltip Redux Component

A generic [TooltipRedux.tsx](/src/tsx/Components/Tooltips/TooltipRedux.tsx) component has been created. This component uses a Redux selector (`getVisibleTooltip`) to get the tooltip element's properties such as text and absolute position on the page. The selected element is the only one in the entire Redux store with the `visible` attribute set to `true`. This component is injected once into the application, specifically in the `AppLayout` component that encapsulates the entire application.

### 3. Tooltip Redux Redux State

The Redux state (cf [store](store.md)) for tooltips is organized as an object, with keys being the IDs of tooltips and values being objects of type `TooltipType`. The `TooltipType` includes attributes like `id`, `text`, `dimensions`, `direction`, `displayTooltip`, `values`, `visible` and `isText`. The `visible` attribute ensures that only one tooltip is visible at a time, this security is ensured by the `TOOLTIP_SHOW` action that sets all `visible` values to false, expect the targeted one that is set to true. The Redux reducer manages these data with actions such as `TOOLTIP_SET`, `TOOLTIP_HIDE`, and `TOOLTIP_SHOW`.

Here is the structure (declared in [models](../src/models/tooltips.ts)) :

```typescript
export const TOP = 'TOP';
export const BOTTOM = 'BOTTOM';
export const LEFT = 'LEFT';
export const RIGHT = 'RIGHT';

export type TooltipDir = typeof TOP | typeof BOTTOM | typeof LEFT | typeof RIGHT;

export type DimensionsType = {
  x: number;
  y: number;
  width: number;
  height: number;
  top: number;
  right: number;
  bottom: number;
  left: number;
};

export type TooltipType = {
  id: string;
  text: string;
  dimensions: DimensionsType;
  direction: TooltipDir;
  displayTooltip?: boolean; // might be false for tooltips displayed under certain conditions
  values?: Record<string, string>;
  visible?: boolean; // Only one of the collection could be visible
  isText?: boolean; // default is false. The Tooltip component use text as a translation key except if the value is pure text alreay. In this case, set isText value to true
};
```

## Using Tooltips

To use a tooltip, you do not need to edit the existing structure. You only need to use the hook, following these steps:

### 1. Import Tooltip Library

```typescript
import useTooltip from '/src/libs/utils/tooltips';
import { LEFT } from '/src/reducers/tooltips';
```

### 2. Apply Tooltip Hook to HTML Element

Apply the useTooltip hook to an HTML element (currently works for `<div>` and `<span>` elements):

```tsx
<span
    ...
    ref={useTooltip(elementUniqueId, {
      text: 'translation.key',
      dir: LEFT,
      displayTooltip: visibleTooltip
    })}
  >
    <Icon icon={ICON_TYPE.CANCEL} size={24} iconClassName="flex-cc" />
  </span>
```

Replace elementUniqueId with a unique identifier for the tooltip. Adjust the text, dir, and displayTooltip properties according to your requirements.

This setup ensures a seamless and efficient tooltip system in the application.
