# Form Management Documentation

This documentation outlines the usage of the react-hook-form library for form management in the application. Refer to the [official documentation](https://react-hook-form.com/) for detailed information.

### Initialization

Initialize a form using the following approach:

```typescript
type IFormInput {
  field: string;
  numberField: number;
  // Additional fields...
}

const {
  control,
  formState: { isValid, errors },
  handleSubmit,
  reset,
  setValue,
  trigger,
} = useForm<IFormInput>({
  defaultValues: {
    field: "",
    numberField: 0,
  },
  mode: "onChange", // "onBlur" | "onChange" | "onSubmit" | "onTouched" | "all" | undefined
});
```

### Validation

Validation is handled using registerOptions. For example:

```typescript
const registerOptions = {
  field: {
    required: 'Field is required',
  },
  numberField: {
    min: {
      value: 0,
      message: 'Should be greater or equal to 0',
    },
    max: {
      value: 999,
      message: 'Should be lower than 1000',
    },
    pattern: {
      value: /^\d*$/,
      message: 'Must be a number',
    },
  },
};
```

### Field Definition

A field is defined as follows:

```typescript
<Controller
  name={name}
  control={control}
  rules={registerOptions[name]}
  render={({ field }) => (
    <Input
      {...field}
      onChange={(value: any) => setValue(name, value)}
    />
  )}
/>
<small className="text-danger">
  {errors && errors[name] && errors[name].message}
</small>
```

### Generic Field Component

A generic field component named [Field](/src/tsx/Components/generic/Form/Field.tsx) has been created. It accepts the following attributes:

- name: string - The field name.
- type: string - The field type.
- className: string - Optional class name.
- control: Control<any> - Form control object.
- registerOptions: any - Validation rules.
- errors: any - Form errors.
- setValue: UseFormSetValue<any> - Form value setter.
- trigger: UseFormTrigger<any> - Trigger for updating errors on field change.
- Component: ElementType - The generic component used for the field (e.g., Input, Dropdown, DatePicker, Checkbox).
- componentProps: any - Properties to pass to the component (e.g., placeholder, dropdown options, min/max values).
- getComponentProps: (field: ControllerRenderProps<FieldValues, string>) => any - A function to dynamically assign properties to the component based on the field.
- iconComponent: ReactNode - Optional icon to display next to the field.
  Usage example:

```typescript
const controllerProps: any = { control, errors, registerOptions, setValue };
<Field
  name="field"
  className="field"
  type="text"
  trigger={trigger}
  Component={Input}
  componentProps={{
    placeholder: 'Field input',
  }}
  getComponentProps={(field: ControllerRenderProps<FieldValues, string>) => ({
    value: field.value,
  })}
  iconComponent={<Icon icon={ICON_TYPE.PEN} iconClassName="flex-cc" />}
  {...controllerProps}
/>;
```

### Generic Form Component

A generic form component named [Form](/src/tsx/Components/generic/Form/Form.tsx) is available. It accepts the following properties:

className: string - Optional class name.
children: ReactNode - Form fields.
onSubmit: any - Function to handle form submission.
reset: UseFormReset<any> - Optional function for resetting the form.
Usage example:

```typescript
<Form className="custom-form" onSubmit={handleSubmit(onSubmit)} reset={reset}>
  {/* Form fields */}
</Form>
```

The Form component displays all child fields and uses the onSubmit function to handle form submission when the "Enter" key is pressed. If a reset function is provided, a "Reset" button is displayed to reset the form to its default values.
