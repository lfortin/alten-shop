import { createSelector } from 'reselect';
import { APPLICATION } from '../constants/reduxTypes';
import { StateInterface } from '../reducers';

const _theme = (state: StateInterface) => state[APPLICATION].theme;
const _locale = (state: StateInterface) => state[APPLICATION].locale;
const _title = (state: StateInterface) => state[APPLICATION].title;
const _isMenuOpen = (state: StateInterface) => state[APPLICATION].menuOpen;

export const getTheme = createSelector(_theme, (theme) => theme);
export const getTitle = createSelector(_title, (title) => title);
export const getLocale = createSelector(_locale, (locale) => locale);
export const getMenuOpen = createSelector(_isMenuOpen, (open) => open);
