import { createSelector } from 'reselect';
import { StateInterface } from '../reducers';
import { TooltipsCollection } from '../reducers/tooltips';
import { TOOLTIPS } from '../constants/reduxTypes';

const _tooltips = (state: StateInterface): TooltipsCollection => state[TOOLTIPS];

export const getTooltips = createSelector(_tooltips, (tooltips: TooltipsCollection) =>
  Object.values(tooltips)
);

export const getVisibleTooltip = createSelector(getTooltips, (tooltips) =>
  tooltips.find((t) => t.visible)
);
