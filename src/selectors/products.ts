import { createSelector } from 'reselect';
import { PRODUCTS } from '../constants/reduxTypes';
import { StateInterface } from '../reducers';
import { _sortBy } from '../libs/utils/array';

interface IProps {
  id: number;
}

const _products = (state: StateInterface) => state[PRODUCTS].collection;
const _filters = (state: StateInterface) => state[PRODUCTS].filters;
const _sort = (state: StateInterface) => state[PRODUCTS].sort;
const _adminSort = (state: StateInterface) => state[PRODUCTS].adminSort;
const _adminOrder = (state: StateInterface) => state[PRODUCTS].adminOrder;
const _selected = (state: StateInterface) => state[PRODUCTS].selected;
const _id = (_state: StateInterface, props: IProps) => props.id;

export const selectProducts = createSelector(
  _products,
  _filters,
  _sort,
  (products, { search, offset, limit }, sort) => {
    const filteredItems = Object.values(products).filter((p) =>
      search ? p.name.toLowerCase().includes(search.toLowerCase()) : true
    );
    const sortedItems = _sortBy(filteredItems, sort);
    return sortedItems.filter((_p, i) => i >= offset && i < offset + limit);
  }
);

export const selectAdminProducts = createSelector(
  _products,
  _adminSort,
  _adminOrder,
  (products, sort, order) => _sortBy(Object.values(products), sort, order)
);

export const selectAllProducts = createSelector(_products, (products) => Object.values(products));

export const getTotalProductsCount = createSelector(
  _products,
  _filters,
  (products, { search }) =>
    Object.values(products).filter((p) =>
      search ? p.name.toLowerCase().includes(search.toLowerCase()) : true
    ).length
);

export const getFilters = createSelector(_filters, (filters) => filters);

export const getSort = createSelector(_sort, (sort) => sort);
export const getAdminSort = createSelector(_adminSort, (sort) => sort);
export const getAdminOrder = createSelector(_adminOrder, (order) => order);

export const getSelectedAdminProducts = createSelector(_selected, (selected) => selected);
export const isSelectedProduct = createSelector(_selected, _id, (selected, id) =>
  selected.includes(id)
);
export const areAllSelectedProducts = createSelector(
  _selected,
  _products,
  (selected, products) =>
    !!Object.values(products).length &&
    Object.values(selected).length === Object.values(products).length
);
