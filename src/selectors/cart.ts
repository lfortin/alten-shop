import { createSelector } from 'reselect';
import { CART, PRODUCTS } from '../constants/reduxTypes';
import { StateInterface } from '../reducers';

interface IProps {
  id: number;
}

const _products = (state: StateInterface) => state[PRODUCTS].collection;
const _items = (state: StateInterface) => state[CART].collection;
const _id = (_state: StateInterface, props: IProps) => props.id;

export const getCartProducts = createSelector(_products, _items, (products, cart) =>
  cart.map((productId) => products[productId])
);

export const getCartItemCount = createSelector(
  _items,
  _id,
  (cart, itemId) => cart.filter((item) => item === itemId).length
);
