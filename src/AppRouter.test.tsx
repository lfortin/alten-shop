import { render, screen } from '@testing-library/react';
import { intl } from './libs/i18n/initReactIntl';
import App from './AppRouter';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(intl.formatMessage({ id: 'app.menu' }));
  expect(linkElement).toBeDefined();
});
