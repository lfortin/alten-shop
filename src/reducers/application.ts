import { Action, SET_LOCALE, SET_MENU_OPEN, SET_THEME, SET_TITLE } from '../actionTypes';
import { LocalePayload, MenuPayload, ThemePayload, TitlePayload } from '../actions/application';
import { LIGHT, Theme } from '../libs/colors';
import { EN, LocaleType } from '../libs/i18n/initReactIntl';

export interface ApplicationReducer {
  state: ApplicationsState;
  action: Action;
}

export interface ApplicationsState {
  theme: Theme;
  title: string;
  locale: LocaleType;
  menuOpen: boolean;
}

const initialState: ApplicationsState = {
  theme: LIGHT,
  title: '',
  locale: EN,
  menuOpen: true,
};

/**
 * This reducer keeps references to certain collections of data
 */
const reducer = (state = initialState, action: Action): ApplicationsState => {
  switch (action.type) {
    case SET_THEME: {
      const { theme } = action.payload as ThemePayload;
      return {
        ...state,
        theme,
      };
    }

    case SET_LOCALE: {
      const { locale } = action.payload as LocalePayload;
      return {
        ...state,
        locale,
      };
    }
    case SET_TITLE: {
      const { title } = action.payload as TitlePayload;
      return {
        ...state,
        title,
      };
    }
    case SET_MENU_OPEN: {
      const { isOpen } = action.payload as MenuPayload;
      return {
        ...state,
        menuOpen: isOpen,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
