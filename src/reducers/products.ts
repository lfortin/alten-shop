import {
  Action,
  SET_PRODUCTS,
  RESET_PRODUCTS,
  SET_PRODUCT,
  ADD_PRODUCTS,
  SET_PRODUCT_FILTER,
  SET_PRODUCT_SORT,
  ADMIN_SELECT,
  ADMIN_SELECT_ALL,
  ADMIN_DELETE,
  ADMIN_EDIT,
  ADMIN_SORT,
} from '../actionTypes';
import {
  ProductPayload,
  ProductsListPayload,
  ProductFilterPayload,
  sortType,
  ProductSortPayload,
  AdminSelectPayload,
  AdminSelectAllPayload,
  AdminDeletePayload,
  AdminEditPayload,
  adminSortType,
  adminSortOrder,
  AdminSortPayload,
} from '../actions/products';
import { PRODUCTS, storeKeys } from '../constants/reduxTypes';
import { _omit } from '../libs/utils/array';
import { Product, ProductFilters } from '../models/products';
import { organizePerKey } from './index';

export interface ProductReducer {
  state: ProductsState;
  action: Action;
}

export interface ProductsState {
  collection: ProductsCollection;
  filters: ProductFilters;
  sort: sortType;
  adminSort: adminSortType;
  adminOrder: adminSortOrder;
  selected: number[];
}

export type ProductsCollection = Record<string, Product>;

const initialStateCollections = {};
const initialStateFilters = {
  search: '',
  limit: 10,
  offset: 0,
};

const initialState: ProductsState = {
  collection: { ...initialStateCollections },
  filters: { ...initialStateFilters },
  sort: 'name',
  adminSort: 'name',
  adminOrder: 'asc',
  selected: [],
};

/**
 * This reducer keeps references to certain collections of data
 */
const reducer = (state = initialState, action: Action): ProductsState => {
  switch (action.type) {
    case ADD_PRODUCTS: {
      const { values } = action.payload as ProductsListPayload;
      return {
        ...state,
        collection: {
          ...state.collection,
          ...organizePerKey(values, storeKeys[PRODUCTS]),
        },
      };
    }
    case SET_PRODUCTS: {
      const { values } = action.payload as ProductsListPayload;
      return {
        ...state,
        collection: organizePerKey(values, storeKeys[PRODUCTS]),
      };
    }
    case SET_PRODUCT: {
      const { id, value }: { id: string; value: Product } = action.payload as ProductPayload;
      return {
        ...state,
        collection: {
          ...state.collection,
          [id]: value,
        },
      };
    }
    case RESET_PRODUCTS: {
      return { ...initialState };
    }
    case SET_PRODUCT_FILTER: {
      const { field, value } = action.payload as ProductFilterPayload;
      return {
        ...state,
        filters: {
          ...state.filters,
          [field]: value,
        },
      };
    }
    case SET_PRODUCT_SORT: {
      const { field } = action.payload as ProductSortPayload;
      return {
        ...state,
        sort: field,
      };
    }

    case ADMIN_SELECT: {
      const { id, selected } = action.payload as AdminSelectPayload;
      let selectedProducts = [...state.selected];
      if (selected) selectedProducts = selectedProducts.concat([id]);
      else selectedProducts = selectedProducts.filter((p) => p !== id);
      return {
        ...state,
        selected: selectedProducts,
      };
    }
    case ADMIN_SELECT_ALL: {
      const { selected } = action.payload as AdminSelectAllPayload;
      const selectedProducts = selected ? Object.values(state.collection).map((p) => p.id) : [];
      return {
        ...state,
        selected: selectedProducts,
      };
    }
    case ADMIN_DELETE: {
      const { ids } = action.payload as AdminDeletePayload;
      const selected = [...state.selected].filter((s) => !ids.includes(s));
      const collection = _omit({ ...state.collection }, ids);
      return {
        ...state,
        collection,
        selected,
      };
    }
    case ADMIN_EDIT: {
      const { id, name } = action.payload as AdminEditPayload;
      return {
        ...state,
        collection: {
          ...state.collection,
          [id]: {
            ...state.collection[id],
            name,
          },
        },
      };
    }
    case ADMIN_SORT: {
      const { field, order } = action.payload as AdminSortPayload;
      return {
        ...state,
        adminSort: field,
        adminOrder: order,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
