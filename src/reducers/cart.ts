import { CART_ADD, CART_RESET, Action } from '../actionTypes';
import { CartPayload } from '../actions/cart';

export interface CartReducer {
  state: CartState;
  action: Action;
}

export interface CartState {
  collection: CartCollection;
}

export type CartCollection = number[];

const initialCartState: CartCollection = [];

const initialState: CartState = {
  collection: [...initialCartState],
};

const reducer = (state = initialState, action: Action): CartState => {
  switch (action.type) {
    case CART_ADD: {
      const { id } = action.payload as CartPayload;
      return {
        ...state,
        collection: state.collection.concat([id]),
      };
    }

    case CART_RESET: {
      return { ...initialState };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
