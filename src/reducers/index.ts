import { configureStore, Reducer, ReducersMapObject } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import application, { ApplicationsState } from './application';
import cart, { CartState } from './cart';
import products, { ProductsState } from './products';
import tooltips, { TooltipsState } from './tooltips';
import { APPLICATION, CART, PRODUCTS, TOOLTIPS } from '../constants/reduxTypes';
import { Action } from '../actionTypes';

export const organizePerKey = <T>(arr: T[], key: string): Record<string, T> =>
  arr.reduce((res, el) => {
    const keyAttr = el[key as keyof T] as string;

    if (keyAttr) {
      return { ...res, [keyAttr]: el };
    }

    return res;
  }, {});

export interface StateInterface {
  [APPLICATION]: ApplicationsState;
  [CART]: CartState;
  [PRODUCTS]: ProductsState;
  [TOOLTIPS]: TooltipsState;
}

export interface StoreInterface {
  [APPLICATION]: (state: ApplicationsState, action: Action) => ApplicationsState;
  [CART]: (state: CartState, action: Action) => CartState;
  [PRODUCTS]: (state: ProductsState, action: Action) => ProductsState;
  [TOOLTIPS]: (state: TooltipsState, action: Action) => TooltipsState;
}

const reducers: ReducersMapObject<StateInterface, Action> = {
  [APPLICATION]: application,
  [CART]: cart,
  [PRODUCTS]: products,
  [TOOLTIPS]: tooltips,
};

const rootReducer: Reducer<StateInterface, Action> = combineReducers(reducers);

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  devTools: !!import.meta.env.DEV,
});

export default rootReducer;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
