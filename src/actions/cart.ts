import { CART_ADD, CART_RESET } from '../actionTypes';

export interface cartItemPayload {
  id: number;
}

export type CartPayload = cartItemPayload;

export const addToCart = (id: number) => ({
  type: CART_ADD,
  payload: { id },
});

export const resetCart = () => ({
  type: CART_RESET,
});
