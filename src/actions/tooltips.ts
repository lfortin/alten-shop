import { TOOLTIP_SET, TOOLTIP_SHOW, TOOLTIP_HIDE, Action } from '../actionTypes';
import { DimensionsType, TOP, TooltipDir, TooltipType } from '../models/tooltips';

interface TooltipAssessor {
  id: string;
}

export type TooltipPayload = TooltipType | TooltipAssessor;

export const setTooltip = (
  id: string,
  text: string,
  dimensions: DimensionsType,
  direction: TooltipDir = TOP,
  displayTooltip = true,
  values?: object,
  isText = false
) => ({
  type: TOOLTIP_SET,
  payload: {
    id,
    text,
    dimensions,
    direction,
    displayTooltip,
    values,
    isText,
  },
});

export const showTooltip = (id: string): Action => ({
  type: TOOLTIP_SHOW,
  payload: {
    id,
  },
});

export const hideTooltip = (id: string) => ({
  type: TOOLTIP_HIDE,
  payload: {
    id,
  },
});
