import {
  ADMIN_SELECT,
  ADMIN_EDIT,
  ADMIN_DELETE,
  ADMIN_SELECT_ALL,
  ADMIN_SORT,
  ADD_PRODUCTS,
  SET_PRODUCT,
  SET_PRODUCTS,
  SET_PRODUCT_FILTER,
  SET_PRODUCT_SORT,
} from '../actionTypes';
import { Product } from '../models/products';

// Products
export interface ProductsListPayload {
  values: Product[];
}
export interface ProductPayload {
  id: string;
  value: Product;
}

export type filterType = 'search' | 'limit' | 'offset';
export interface ProductFilterPayload {
  field: filterType;
  value: string | number;
}

export type sortType = 'name' | 'price' | 'category' | 'rating';
export interface ProductSortPayload {
  field: sortType;
}

export type ProductsPayload =
  | ProductsListPayload
  | ProductPayload
  | ProductFilterPayload
  | ProductSortPayload;

export const addProducts = (values: Product[]) => ({
  type: ADD_PRODUCTS,
  payload: { values },
});

export const setProducts = (values: Product[]) => ({
  type: SET_PRODUCTS,
  payload: { values },
});

export const setProduct = (id: string, value: Product) => ({
  type: SET_PRODUCT,
  payload: { id, value },
});

export const setProductFilter = (field: filterType, value: number | string) => ({
  type: SET_PRODUCT_FILTER,
  payload: { field, value },
});

export const setProductSort = (field: sortType) => ({
  type: SET_PRODUCT_SORT,
  payload: { field },
});

// Admin
export interface AdminSelectPayload {
  id: number;
  selected: boolean;
}
export interface AdminSelectAllPayload {
  selected: boolean;
}

export interface AdminAddPayload {
  code: string;
  name: string;
}
export interface AdminEditPayload {
  id: number;
  name: string;
}
export interface AdminDeletePayload {
  ids: number[];
}

export type adminSortType = 'name' | 'code';
export type adminSortOrder = 'asc' | 'desc';
export interface AdminSortPayload {
  field: adminSortType;
  order: adminSortOrder;
}

export type AdminPayload =
  | AdminSelectPayload
  | AdminSelectAllPayload
  | AdminAddPayload
  | AdminEditPayload
  | AdminDeletePayload
  | AdminSortPayload;

export const selectAdminProduct = (id: number, selected: boolean) => ({
  type: ADMIN_SELECT,
  payload: { id, selected },
});

export const selectAdminAllProducts = (selected: boolean) => ({
  type: ADMIN_SELECT_ALL,
  payload: { selected },
});

export const editAdminProduct = (id: number, name: string) => ({
  type: ADMIN_EDIT,
  payload: { id, name },
});

export const deleteAdminProducts = (ids: number[]) => ({
  type: ADMIN_DELETE,
  payload: { ids },
});

export const sortAdminProduct = (field: adminSortType, order: adminSortOrder) => ({
  type: ADMIN_SORT,
  payload: { field, order },
});
