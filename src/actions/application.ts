import { SET_LOCALE, SET_MENU_OPEN, SET_THEME, SET_TITLE } from '../actionTypes';
import { Theme } from '../libs/colors';
import { LocaleType } from '../libs/i18n/initReactIntl';

export interface ThemePayload {
  theme: Theme;
}
export interface LocalePayload {
  locale: LocaleType;
}
export interface TitlePayload {
  title: string;
}
export interface MenuPayload {
  isOpen: boolean;
}

export type ApplicationPayload = LocalePayload | MenuPayload | ThemePayload | TitlePayload;

export const setTheme = (theme: Theme) => ({
  type: SET_THEME,
  payload: { theme },
});
export const setlocale = (locale: LocaleType) => ({
  type: SET_LOCALE,
  payload: { locale },
});

export const setTitle = (title: string) => ({
  type: SET_TITLE,
  payload: { title },
});

export const setMenuOpen = (isOpen: boolean) => ({
  type: SET_MENU_OPEN,
  payload: { isOpen },
});
