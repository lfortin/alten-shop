import {
  addProducts,
  deleteAdminProducts,
  editAdminProduct,
  setProducts,
} from '../actions/products';
import { store } from '../reducers';
import products from '../assets/products.json' assert { type: 'json' };
import { Product } from '../models/products';
import { PRODUCTS } from '../constants/reduxTypes';
import dispatcher from '../libs/utils/dispatcher';
import { success } from '../libs/Notifier';

const currentState = store.getState();

export const fetchProducts = async (): Promise<void> => {
  const { dispatch } = dispatcher;
  return new Promise((resolve) => {
    setTimeout(() => {
      if (dispatch) {
        const productsList: Product[] = products.data;
        dispatch(setProducts(productsList));
      }
      resolve();
    }, 300);
  });
};

export const addProduct = async (code: string, name: string): Promise<void> => {
  const { dispatch } = dispatcher;
  return new Promise((resolve) => {
    setTimeout(() => {
      success('admin.adding');
      if (dispatch) {
        const existingProducts = Object.values(currentState[PRODUCTS].collection);
        const newId = Math.max(...existingProducts.map((p) => p.id)) + 1;
        const newProduct: Product = {
          id: newId,
          code,
          name,
          description: '',
          price: 0,
          quantity: 1,
          inventoryStatus: 'INSTOCK',
          category: 'Accessories',
        };
        dispatch(addProducts([newProduct]));
      }
      resolve();
    }, 300);
  });
};

export const editProduct = async (id: number, name: string): Promise<void> => {
  const { dispatch } = dispatcher;
  return new Promise((resolve) => {
    setTimeout(() => {
      success('admin.edition');
      if (dispatch) {
        dispatch(editAdminProduct(id, name));
      }
      resolve();
    }, 300);
  });
};

export const deleteProducts = async (ids: number[]): Promise<void> => {
  const { dispatch } = dispatcher;
  return new Promise((resolve) => {
    setTimeout(() => {
      success('admin.deletion');
      if (dispatch) {
        dispatch(deleteAdminProducts(ids));
      }
      resolve();
    }, 300);
  });
};
