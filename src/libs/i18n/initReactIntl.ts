import { createIntl, createIntlCache } from 'react-intl';
import en from './en.json' assert { type: 'json' };
import fr from './fr.json' assert { type: 'json' };

export const EN = 'EN';
export const FR = 'FR';

export type LocaleType = typeof EN | typeof FR;
export const defaultLocale: LocaleType = EN;

const defaultMsg = defaultLocale === EN ? en : fr;

const cache = createIntlCache();

export const intl = createIntl(
  {
    locale: defaultLocale,
    messages: defaultMsg,
  },
  cache
);

export const switchLocale = (locale: LocaleType) => {
  switch (locale) {
    case EN:
      intl.messages = en;
      break;
    case FR:
      intl.messages = fr;
      break;
    default:
      intl.messages = defaultMsg;
  }

  intl.locale = locale;
};
