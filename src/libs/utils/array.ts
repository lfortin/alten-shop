export const _omit = <T extends Record<string, unknown>>(object: T, keys: (keyof T)[]): T => {
  const rep: T = { ...object };

  keys.forEach((attr) => {
    if (Object.keys(rep).includes(attr.toString())) {
      delete rep[attr];
    }
  });

  return rep;
};

export const _sortBy = <T>(arr: T[], attr: keyof T, order = 'asc'): T[] => {
  const sortedArray = [...arr];
  const compare = (a: T, b: T) => {
    if (a[attr] < b[attr]) return order === 'asc' ? -1 : 1;
    if (a[attr] > b[attr]) return order === 'asc' ? 1 : -1;
    return 0;
  };
  sortedArray.sort(compare);
  return sortedArray;
};
