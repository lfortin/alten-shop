/* eslint-disable react-hooks/exhaustive-deps */
import { useRef, useEffect } from 'react';
import { setTooltip, hideTooltip, showTooltip } from '../../actions/tooltips';
import { TooltipDir } from '../../models/tooltips';
import dispatcher from './dispatcher';

/**
 *	Detects the hover and leave of an element with a tooltip
 *
 * How to use it :
 * import useTooltip from "../../../libs/utils/tooltips";
 * import { TOP } from "../../../models/tooltips";
 * <div ref={useTooltip(elementUniqueId, { text: 'translation.key', dir: TOP, ... })}></div>
 * props :
 *  - id: the tooltip unique id
 *  - props: an object with all the necessary props of the tooltip
 *    text: the translation key of the tooltip
 *    dir: the direction to display the tooltip (TOP by default)
 *    displayTooltip: a boolean to force a tooltip te be hidden (for instance in a dropdown menu)
 *    values: the values that are used in the translation keys
 *    isText: false by default. If true, the Tooltip renderer will use text as a pure text and not a translation key
 *
 * If a hook rerender error appear : create a new component at the top, and import it in your component.
 * Example :
 *
 * const CancelAction = ({ cancel, instanceId }) => (
  <span
    className="icon"
    onClick={cancel}
    ref={useTooltip('icon cancel', {
      text: 'button.cancel',
      dir: 'LEFT,
    })}
  >
    <Icon icon={ICON_TYPE.CANCEL} size={24} iconClassName="flex-cc" />
  </span>
);
*/
export type TooltipElement<T extends HTMLElement = HTMLSpanElement> = {
  contains: (element: HTMLElement) => boolean;
  fromElement: HTMLElement;
} & T;

interface Tooltip {
  text: string;
  dir: TooltipDir;
  displayTooltip?: boolean;
  values?: object;
  visible?: boolean;
  isText?: boolean;
}

type HoverLeaveMouseEvent = MouseEvent & {
  fromElement?: HTMLElement;
  toElement?: HTMLElement;
};

type TooltipHook<T extends HTMLElement> = (
  id: string,
  props: Tooltip
) => React.MutableRefObject<TooltipElement<T> | null>;

const useTooltip: TooltipHook<HTMLDivElement> = (id, props) => {
  const innerRef = useRef<TooltipElement<HTMLDivElement> | null>(null);

  const recalculateShow = (e: HoverLeaveMouseEvent): boolean => {
    const isDisplayed = props.displayTooltip ?? true;

    if (
      innerRef.current &&
      id &&
      innerRef.current.contains(e.target as HTMLElement) &&
      !innerRef.current.contains(e.fromElement!) &&
      isDisplayed
    ) {
      return true;
    }
    return false;
  };

  const hideCdt = (e: HoverLeaveMouseEvent): boolean => {
    const isDisplayed = props.displayTooltip ?? true;
    if (
      innerRef.current &&
      id &&
      innerRef.current.contains(e.target as HTMLElement) &&
      !innerRef.current.contains(e.toElement!) &&
      isDisplayed
    ) {
      return true;
    }
    return false;
  };

  const handleHover = (e: MouseEvent): void => {
    const { dispatch } = dispatcher;

    if (recalculateShow(e)) {
      setTip();
      dispatch?.(showTooltip(id));
    }
  };

  const handleLeave = (e: MouseEvent): void => {
    const { dispatch } = dispatcher;
    if (hideCdt(e)) {
      dispatch?.(hideTooltip(id));
    }
  };

  const handleClick = (): void => {
    const { dispatch } = dispatcher;
    dispatch?.(hideTooltip(id));
  };

  useEffect(() => {
    if (innerRef.current) {
      innerRef.current.addEventListener('mouseover', handleHover);
      innerRef.current.addEventListener('mouseout', handleLeave);
      innerRef.current.addEventListener('click', handleClick);
    }
    return () => {
      if (innerRef.current) {
        innerRef.current.removeEventListener('mouseover', handleHover);
        innerRef.current.removeEventListener('mouseout', handleLeave);
        innerRef.current.removeEventListener('click', handleClick);
      }
    };
  }, [props.text, props.displayTooltip, props.dir]);

  useEffect(() => {
    setTip();
  }, [props.displayTooltip]);

  const setTip = (): void => {
    const { text, dir, displayTooltip = true, values, isText } = props;

    const { dispatch } = dispatcher;
    if (text === '') return;
    if (innerRef && innerRef.current) {
      const dimensions = innerRef.current.getBoundingClientRect();
      dispatch?.(setTooltip(id, text, dimensions, dir, displayTooltip, values, isText));
    }
  };

  return innerRef;
};

export default useTooltip;
