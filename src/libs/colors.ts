export const LIGHT = 'LIGHT';
export const DARK = 'DARK';

export type Theme = typeof LIGHT | typeof DARK;
export const defaultTheme: Theme = LIGHT;

interface IColor {
  backgroundColor: string;
  secondaryBackgroundColor: string;
  textColor: string;
  menuColor: string;
  menuAccentColor: string;
  menuTextColor: string;
  borderColor: string;
  accentColor: string;
}

const colorsTab: Record<keyof IColor, string> = {
  backgroundColor: '--background-color',
  secondaryBackgroundColor: '--secondary-background-color',
  textColor: '--text-color',
  menuColor: '--menu-color',
  menuAccentColor: '--menu-accent-color',
  menuTextColor: '--menu-text-color',
  borderColor: '--border-color',
  accentColor: '--accent-color',
};

const lightColors: IColor = {
  backgroundColor: '#f5f5f5',
  secondaryBackgroundColor: '#ffffff',
  textColor: '#232a2f',
  menuColor: '#2c343b',
  menuAccentColor: '#232a2f',
  menuTextColor: '#c3c3c3',
  borderColor: '#e4e4e4',
  accentColor: '#2251b1',
};

const darkColors: IColor = {
  backgroundColor: '#232a2f',
  secondaryBackgroundColor: '#2c343b',
  textColor: '#ffffff',
  menuColor: '#1a2024',
  menuAccentColor: '#2c343b',
  menuTextColor: '#c3c3c3',
  borderColor: '#1a1a1a',
  accentColor: '#2251b1',
};

export type ThemeAttr = keyof IColor;

export const changeTheme = (theme: Theme) => {
  const newColors: IColor = theme === LIGHT ? { ...lightColors } : { ...darkColors };
  Object.entries(newColors).forEach(([key, color]: [string, string]) => {
    const colorKey = key as ThemeAttr;
    document.documentElement.style.setProperty(colorsTab[colorKey], color);
  });
};
