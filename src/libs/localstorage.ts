export const KSESSION = 'ksession';
export type LocalStorageKeys = typeof KSESSION;

export const keys: LocalStorageKeys[] = [KSESSION];

export const set = (key: LocalStorageKeys, value: string) => {
  // When setting an undefined, localStorage stores the string version "undefined", which is not falsy anymore.
  // We enforce key deletion when value is undefined or null
  if (value === undefined || value === null) {
    window.localStorage.removeItem(key);
  } else {
    window.localStorage.setItem(key, value);
  }
};
export const get = (key: LocalStorageKeys) => window.localStorage.getItem(key);

export const remove = (key: LocalStorageKeys) => window.localStorage.removeItem(key);

export const clear = () => window.localStorage.clear();
