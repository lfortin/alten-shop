import { ApplicationPayload } from './actions/application';
import { CartPayload } from './actions/cart';
import { AdminPayload, ProductsPayload } from './actions/products';
import { TooltipPayload } from './actions/tooltips';
export interface Action {
  type: string;
  payload: AdminPayload | ApplicationPayload | CartPayload | ProductsPayload | TooltipPayload;
}

export const SET_THEME = 'SET_THEME';
export const SET_LOCALE = 'SET_LOCALE';
export const SET_TITLE = 'SET_TITLE';
export const SET_MENU_OPEN = 'SET_MENU_OPEN';

export const ADMIN_SELECT = 'ADMIN_SELECT';
export const ADMIN_SELECT_ALL = 'ADMIN_SELECT_ALL';
export const ADMIN_EDIT = 'ADMIN_EDIT';
export const ADMIN_DELETE = 'ADMIN_DELETE';
export const ADMIN_SORT = 'ADMIN_SORT';

export const ADD_PRODUCTS = 'ADD_PRODUCTS';
export const SET_PRODUCTS = 'SET_PRODUCTS';
export const SET_PRODUCT = 'SET_PRODUCT';
export const RESET_PRODUCTS = 'RESET_PRODUCTS';

export const SET_PRODUCT_FILTER = 'SET_PRODUCT_FILTER';
export const SET_PRODUCT_SORT = 'SET_PRODUCT_SORT';

export const TOOLTIP_SET = 'TOOLTIP_SET';
export const TOOLTIP_SHOW = 'TOOLTIP_SHOW';
export const TOOLTIP_HIDE = 'TOOLTIP_HIDE';

export const CART_ADD = 'CART_ADD';
export const CART_RESET = 'CART_RESET';
