export const TOP = 'TOP';
export const BOTTOM = 'BOTTOM';
export const LEFT = 'LEFT';
export const RIGHT = 'RIGHT';

export type TooltipDir = typeof TOP | typeof BOTTOM | typeof LEFT | typeof RIGHT;

export interface DimensionsType {
  x: number;
  y: number;
  width: number;
  height: number;
  top: number;
  right: number;
  bottom: number;
  left: number;
}

export interface TooltipType {
  id: string;
  text: string;
  dimensions: DimensionsType;
  direction: TooltipDir;
  displayTooltip?: boolean; // might be false for tooltips displayed under certain conditions
  values?: Record<string, string>;
  visible?: boolean; // Only one of the collection could be visible
  isText?: boolean; // default is false. The Tooltip component use text as a translation key except if the value is pure text alreay. In this case, set isText value to true
}
