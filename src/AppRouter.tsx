import { FC, useEffect } from 'react';
import { Location, Navigate, Routes, Route, useLocation, useNavigate } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { IntlProvider } from 'react-intl';
import './App.css';
// redux
import { StateInterface } from './reducers';
import { getLocale } from './selectors/application';
// components
import Admin from './tsx/Views/Admin';
import Products from './tsx/Views/Products';
import AppLayout from './tsx/Components/AppLayout/AppLayout';
// libs
import { defaultTheme, changeTheme } from './libs/colors';
import dispatcher from './libs/utils/dispatcher';
import { history } from './libs/utils/navigation';
import { LocaleType, defaultLocale, intl, switchLocale } from './libs/i18n/initReactIntl';

const AppRouter: FC<Props> = ({ locale }) => {
  const location: Location = useLocation();

  history.navigate = useNavigate();
  history.location = location;

  const dispatch = useDispatch();
  if (!dispatcher.dispatch) {
    dispatcher.dispatch = dispatch;
  }

  useEffect(() => {
    changeTheme(defaultTheme);
  }, []);

  useEffect(() => {
    switchLocale(locale ?? defaultLocale);
  }, [locale]);

  return (
    <div className="app-wrapper">
      <IntlProvider locale={locale ?? defaultLocale} messages={intl.messages}>
        <Routes>
          <Route element={<AppLayout location={location} />}>
            <Route path="/" element={<Navigate to="/admin/products" />} />
            <Route path="/admin/products" element={<Admin />}></Route>
            <Route path="/products" element={<Products />}></Route>
            <Route path="*" element={<Navigate to="/admin/products" />} />
          </Route>
        </Routes>
      </IntlProvider>
    </div>
  );
};

interface Props {
  locale?: LocaleType;
}

const mapStateToProps = (state: StateInterface) => ({
  locale: getLocale(state),
});

export default connect(mapStateToProps)(AppRouter);
