export const APPLICATION = 'application';
export const CART = 'cart';
export const PRODUCTS = 'products';
export const TOOLTIPS = 'tooltips';

export const storeKeys = {
  [PRODUCTS]: 'id',
  [TOOLTIPS]: 'id',
};
