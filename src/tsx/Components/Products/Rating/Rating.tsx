import { FC } from 'react';
import MuiRating from '@mui/material/Rating';
const Rating: FC<Props> = ({ stars }) => {
  return (
    <div className="stars">
      <MuiRating value={stars} disabled />
    </div>
  );
};

interface Props {
  stars: number;
}

export default Rating;
