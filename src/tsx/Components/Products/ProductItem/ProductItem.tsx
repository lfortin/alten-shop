import { FC } from 'react';
import { connect, useDispatch } from 'react-redux';
import './ProductList.css';
import './ProductTile.css';
import { Product } from '../../../../models/products';
import { addToCart } from '../../../../actions/cart';
import { StateInterface } from '../../../../reducers';
import { getCartItemCount } from '../../../../selectors/cart';
import Rating from '../Rating';
import Button from '../../generic/Button';
import Icon from '../../Icon';
import { ICON_TYPE } from '../../Icon/Icon';

const ProductItem: FC<Props> = ({ className, product, inCart = 0 }) => {
  const disptach = useDispatch();
  const addItemToCart = () => void disptach(addToCart(product.id));
  return (
    <div className={className}>
      <div className="category">
        <Icon icon={ICON_TYPE.TAG} iconClassName="mr5" />
        {product.category}
      </div>
      <div className="inventoryStatus">{product.inventoryStatus}</div>
      <div className="name">{product.name}</div>
      <div className="description">{product.description}</div>
      <Rating stars={product.rating ?? 0} />
      <div className="pricing">
        <div className="price">{product.price}</div>
        <Button className="ml5" onClick={addItemToCart}>
          <Icon icon={ICON_TYPE.PRODUCTS} />
          {!!inCart && <div className="cart-item-count">{inCart}</div>}
        </Button>
      </div>
    </div>
  );
};

interface Props {
  className: string;
  inCart?: number;
  product: Product;
}

const mapStateToProps = (state: StateInterface, props: Props) => ({
  inCart: getCartItemCount(state, props.product),
});

export default connect(mapStateToProps)(ProductItem);
