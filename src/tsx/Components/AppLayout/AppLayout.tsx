import { FC } from 'react';
import { Location, Outlet } from 'react-router-dom';
import { useIntl } from 'react-intl';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './AppLayout.css';
// redux
import { StateInterface } from '../../../reducers';
import { getTitle } from '../../../selectors/application';
// Components
import Header from '../Header';
import Menu from '../Menu';
import TooltipRedux from '../Tooltips';

const AppLayout: FC<Props> = ({ location, title, ...rest }) => {
  const intl = useIntl();

  if (title) document.title = intl.formatMessage({ id: title });

  return (
    <div className="app flex">
      <Menu location={location} />
      <div className="grow flex-col app-content">
        <Header location={location} />
        <Outlet context={{ ...rest }} />
      </div>
      <TooltipRedux />
      <ToastContainer />
    </div>
  );
};

interface Props {
  location: Location;
  title?: string;
}

const mapStateToProps = (state: StateInterface) => ({
  title: getTitle(state),
});
export default connect(mapStateToProps)(AppLayout);
