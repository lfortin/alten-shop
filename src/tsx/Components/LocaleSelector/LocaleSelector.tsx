import { FC } from 'react';
import { useIntl } from 'react-intl';
import { connect, useDispatch } from 'react-redux';
import './LocaleSelector.css';
// redux
import { setlocale } from '../../../actions/application';
import { StateInterface } from '../../../reducers';
import { getLocale } from '../../../selectors/application';
// components
import Dropdown from '../generic/Dropdown';
import { DropdownOption, SelectedOption } from '../generic/Dropdown/Dropdown';
import { EN, FR, LocaleType, defaultLocale, switchLocale } from '../../../libs/i18n/initReactIntl';

const LocaleSelector: FC<Props> = ({ isOpen, locale }) => {
  const intl = useIntl();
  const dispatch = useDispatch();

  const changeLocale = (value: SelectedOption) => {
    dispatch(setlocale(value as LocaleType)); // redux
    switchLocale(value as LocaleType); // i18n manager
  };

  const enLabel = intl.formatMessage({
    id: `locale.en${isOpen ? '' : '_short'}`,
  });
  const frLabel = intl.formatMessage({
    id: `locale.fr${isOpen ? '' : '_short'}`,
  });

  const localeOptions: DropdownOption[] = [
    { value: EN, label: enLabel },
    { value: FR, label: frLabel },
  ];

  return (
    <Dropdown
      className="locale-selector"
      options={localeOptions}
      selectedOption={locale ?? defaultLocale}
      onChange={changeLocale}
      width={isOpen ? 8 : 4}
    />
  );
};

interface Props {
  isOpen?: boolean;
  locale?: LocaleType;
}

const mapStateToProps = (state: StateInterface) => ({
  locale: getLocale(state),
});

export default connect(mapStateToProps)(LocaleSelector);
