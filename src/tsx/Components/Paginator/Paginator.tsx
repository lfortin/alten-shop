import { FC } from 'react';
import { useDispatch } from 'react-redux';
import Pagination from '@mui/material/Pagination';
import './Paginator.css';
import { setProductFilter } from '../../../actions/products';
import { ProductFilters } from '../../../models/products';
import Dropdown, { DropdownOption, SelectedOption } from '../generic/Dropdown';
import { FormattedMessage } from 'react-intl';

const Paginator: FC<Props> = ({ filters, count }) => {
  const dispatch = useDispatch();
  const { limit, offset } = filters;

  const countOptions: DropdownOption[] = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
    { value: 50, label: '50' },
  ];

  const onLimitChange = (value: SelectedOption) => {
    dispatch(setProductFilter('offset', 0));
    dispatch(setProductFilter('limit', Number(value)));
  };

  const onPageChange = (_e: React.ChangeEvent<unknown>, page: number) => {
    dispatch(setProductFilter('offset', (page - 1) * limit));
  };

  const lastItem = Math.min(offset + limit, count);
  const firstItem = Math.min(offset + offset + 1, count);

  return (
    <div className="paginator">
      <span>
        <FormattedMessage
          id="paginator.entries"
          values={{ first: firstItem, last: lastItem, total: count }}
        />
      </span>
      <Pagination count={Math.ceil(count / limit)} onChange={onPageChange} />
      <Dropdown options={countOptions} selectedOption={limit} onChange={onLimitChange} width={4} />
    </div>
  );
};

interface Props {
  filters: ProductFilters;
  count: number;
}

export default Paginator;
