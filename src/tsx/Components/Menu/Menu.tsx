/* eslint-disable react-hooks/exhaustive-deps */
import { FC, ReactNode, memo, useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Location } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import './Menu.css';
// redux
import { setMenuOpen } from '../../../actions/application';
import { StateInterface } from '../../../reducers';
import { getMenuOpen } from '../../../selectors/application';
import { RIGHT } from '../../../models/tooltips';
// components
import LinkTo from './LinkTo';
import LocaleSelector from '../LocaleSelector';
import Icon from '../Icon';
import { IconType, ICON_TYPE } from '../Icon/Icon';
// libs
import useTooltip from '../../../libs/utils/tooltips';

const OPEN_MENU_MIN_WIDTH = 750;

const LinkBtn: FC<LinkBtnProps> = ({ children, active, label, navbarMinimized, tooltipId }) => (
  <div
    ref={useTooltip(tooltipId, {
      text: label,
      displayTooltip: navbarMinimized,
      dir: RIGHT,
    })}
    className={`link-wrapper ${active ? 'active' : ''}`}
  >
    {children}
  </div>
);

interface LinkBtnProps {
  active: boolean;
  tooltipId: string;
  label: string;
  children: ReactNode;
  navbarMinimized: boolean;
}

const Menu: FC<Props> = ({ isOpen, location }) => {
  const [isWideEnough, setWideEnough] = useState(window.innerWidth >= OPEN_MENU_MIN_WIDTH);
  const intl = useIntl();
  const dispatch = useDispatch();

  useEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [isWideEnough]);

  const msg: string = intl.formatMessage({
    id: `app.menu${isOpen ? '' : '_small'}`,
  });
  const { pathname } = location;

  const menuIcon: IconType = isOpen ? ICON_TYPE.FOLD_MENU : ICON_TYPE.MENU;

  const onChangeMenu = () => {
    dispatch(setMenuOpen(!isOpen));
  };

  const handleResize = () => {
    const wideEnough = window.innerWidth >= OPEN_MENU_MIN_WIDTH;
    if (wideEnough !== isWideEnough || wideEnough !== isOpen) {
      setWideEnough(wideEnough);
      dispatch(setMenuOpen(wideEnough));
    }
  };

  interface MenuElement {
    linkTo: string;
    className: string;
    displayMenuTitle: boolean;
    isActive: boolean;
    menuIcon: IconType;
    text: string;
    testId: string;
  }

  const menuElements: MenuElement[] = [
    {
      linkTo: '/products',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: pathname === '/products',
      menuIcon: ICON_TYPE.PRODUCTS,
      text: 'products',
      testId: 'to-products',
    },
    {
      linkTo: '/admin/products',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: pathname === '/admin/products',
      menuIcon: ICON_TYPE.ADMIN,
      text: 'admin',
      testId: 'to-admin',
    },
  ];

  return (
    <div className={`menu ${isOpen ? 'open' : ''}`}>
      <div className="title">
        <span>{msg}</span>
        <div className="icon" onClick={onChangeMenu}>
          <Icon icon={menuIcon} size={48} />
        </div>
      </div>
      <div className="menu-links grow">
        {menuElements.map((e: MenuElement) => (
          <LinkBtn
            key={e.text}
            active={e.isActive}
            label={`pages.${e.text}`}
            navbarMinimized={!isOpen}
            tooltipId={`nav-${e.text}`}
          >
            <LinkTo
              to={e.linkTo}
              className={e.className}
              displayMenuTitle={!!isOpen}
              isActive={e.isActive}
              menuIcon={e.menuIcon}
              messageId={`pages.${e.text}`}
              testId={e.testId}
            ></LinkTo>
          </LinkBtn>
        ))}
      </div>
      <div className="menu-bottom">
        <LocaleSelector isOpen={isOpen} />
      </div>
    </div>
  );
};

interface Props {
  isOpen?: boolean;
  location: Location;
}

const mapStateToProps = (state: StateInterface) => ({
  isOpen: getMenuOpen(state),
});

const areEquals = (props: Props, nextProps: Props) =>
  !!(props.location.pathname === nextProps.location.pathname) && props.isOpen === nextProps.isOpen;

export default connect(mapStateToProps)(memo(Menu, areEquals));
