import { FC } from 'react';
import { connect, useDispatch } from 'react-redux';
import './ThemeSelector.css';
// redux
import { setTheme } from '../../../actions/application';
import { StateInterface } from '../../../reducers';
import { getTheme } from '../../../selectors/application';
// components
import Icon from '../Icon';
import { ICON_TYPE, IconType } from '../Icon/Icon';
import { DARK, LIGHT, Theme, changeTheme } from '../../../libs/colors';

const ThemeSelector: FC<Props> = ({ theme }) => {
  const dispatch = useDispatch();

  const switchTheme = (): void => {
    const newTheme: Theme = theme === LIGHT ? DARK : LIGHT;
    dispatch(setTheme(newTheme)); // redux
    changeTheme(newTheme); // colors library
  };

  const isLight = theme === LIGHT;
  const icon: IconType = isLight ? ICON_TYPE.LIGHT_MODE : ICON_TYPE.DARK_MODE;

  return (
    <div className="box theme-selector" onClick={switchTheme}>
      <div className={`switch ${isLight ? 'active' : ''}`}>
        <div className="slider">
          <Icon icon={icon} iconClassName="flex" />
        </div>
      </div>
    </div>
  );
};

interface Props {
  theme?: Theme;
}

const mapStateToProps = (state: StateInterface) => ({
  theme: getTheme(state),
});

export default connect(mapStateToProps)(ThemeSelector);
