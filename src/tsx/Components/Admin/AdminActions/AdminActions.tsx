import { FC } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { deleteProducts } from '../../../../api/products';
import { StateInterface } from '../../../../reducers';
import { getSelectedAdminProducts } from '../../../../selectors/products';
import Button from '../../generic/Button';
import Icon from '../../Icon';
import { ICON_TYPE } from '../../Icon/Icon';

const AdminActions: FC<Props> = ({ isAdding, selected = [], onAdd }) => {
  const deleteSelected = () => {
    void deleteProducts(selected ?? []);
  };

  return (
    <div className="flex w100">
      <Button disabled={isAdding} onClick={onAdd}>
        <Icon icon={ICON_TYPE.ADD} />
        <FormattedMessage id="admin.new" />
      </Button>
      <Button onClick={deleteSelected} disabled={!selected.length}>
        <Icon icon={ICON_TYPE.DELETE} />
        <FormattedMessage id="admin.delete" />
      </Button>
    </div>
  );
};

interface Props {
  isAdding: boolean;
  selected?: number[];
  onAdd: () => void;
}

const mapStateToProps = (state: StateInterface) => ({
  selected: getSelectedAdminProducts(state),
});

export default connect(mapStateToProps)(AdminActions);
