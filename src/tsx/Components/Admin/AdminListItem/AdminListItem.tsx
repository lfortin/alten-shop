import { FC, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import './AdminListItem.css';
import { Product } from '../../../../models/products';
// redux
import { selectAdminProduct } from '../../../../actions/products';
import { deleteProducts, editProduct } from '../../../../api/products';
import { StateInterface } from '../../../../reducers';
import { isSelectedProduct } from '../../../../selectors/products';
// components
import { ActionCol, CheckboxCol, CodeCol, NameCol } from '../Table';
import Checkbox from '../../generic/Checkbox';
import Input from '../../generic/Input';
import Icon from '../../Icon';
import { ICON_TYPE } from '../../Icon/Icon';

const AdminListItem: FC<Props> = ({ isSelected, product }) => {
  const dispatch = useDispatch();
  const [editing, setEditing] = useState(false);
  const [name, setName] = useState('');

  const onSelect = (value: boolean) => {
    dispatch(selectAdminProduct(product.id, value));
  };

  const onEdit = () => {
    setName(product.name);
    setEditing(true);
  };

  const onCancelEdit = () => setEditing(false);

  const onUpdateName = () => {
    void editProduct(product.id, name)
      .then(() => setEditing(false))
      .catch(() => setEditing(false));
  };

  const deleteProduct = () => {
    void deleteProducts([product.id]);
  };

  return (
    <div className="admin-list-item">
      <CheckboxCol>
        <Checkbox checked={isSelected} onChange={onSelect} />
      </CheckboxCol>
      <CodeCol>{product.code}</CodeCol>
      <NameCol>
        {editing ? (
          <Input value={name} onChange={(v) => setName(v)} placeholder="app.menu" />
        ) : (
          <>{product.name}</>
        )}
      </NameCol>
      <ActionCol className="flex justify-center">
        {editing ? (
          <>
            <div onClick={onUpdateName}>
              <Icon icon={ICON_TYPE.CHECKED} />
            </div>
            <div onClick={onCancelEdit}>
              <Icon icon={ICON_TYPE.CANCEL} iconClassName="red" />
            </div>
          </>
        ) : (
          <>
            <div onClick={onEdit}>
              <Icon icon={ICON_TYPE.EDIT} />
            </div>
            <div onClick={deleteProduct}>
              <Icon icon={ICON_TYPE.DELETE} iconClassName="red" />
            </div>
          </>
        )}
      </ActionCol>
    </div>
  );
};

interface Props {
  isSelected?: boolean;
  product: Product;
}

const mapStateToProps = (state: StateInterface, props: Props) => ({
  isSelected: isSelectedProduct(state, props.product),
});

export default connect(mapStateToProps)(AdminListItem);
