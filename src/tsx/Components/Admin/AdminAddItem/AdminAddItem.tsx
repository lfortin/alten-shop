import { FC, useState } from 'react';
import './AdminAddItem.css';
// components
import { ActionCol, CheckboxCol, CodeCol, NameCol } from '../Table';
import Input from '../../generic/Input';
import Icon from '../../Icon';
import { ICON_TYPE } from '../../Icon/Icon';

const AdminAddItem: FC<Props> = ({ onAdd, onCancel }) => {
  const [code, setCode] = useState('');
  const [name, setName] = useState('');

  const onSave = () => {
    void onAdd(code, name);
  };

  return (
    <div className="admin-add-item">
      <CheckboxCol />
      <CodeCol>
        <Input value={code} onChange={setCode} placeholder="admin.new.code" />
      </CodeCol>
      <NameCol>
        <Input value={name} onChange={setName} placeholder="admin.new.name" />
      </NameCol>
      <ActionCol className="flex justify-center">
        <>
          <div onClick={onSave}>
            <Icon icon={ICON_TYPE.CHECKED} />
          </div>
          <div onClick={onCancel}>
            <Icon icon={ICON_TYPE.CANCEL} iconClassName="red" />
          </div>
        </>
      </ActionCol>
    </div>
  );
};

interface Props {
  onAdd: (code: string, name: string) => void;
  onCancel: () => void;
}

export default AdminAddItem;
