import { FC } from 'react';
import { connect, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import './AdminHeader.css';
import {
  adminSortOrder,
  adminSortType,
  selectAdminAllProducts,
  sortAdminProduct,
} from '../../../../actions/products';
import { StateInterface } from '../../../../reducers';
import {
  areAllSelectedProducts,
  getAdminOrder,
  getAdminSort,
  getTotalProductsCount,
} from '../../../../selectors/products';
import { ActionCol, CheckboxCol, CodeCol, NameCol } from '../Table';
import Checkbox from '../../generic/Checkbox';
import Icon, { ICON_TYPE } from '../../Icon/Icon';

const AdminHeader: FC<Props> = ({ isChecked, products, sort, order }) => {
  const dispatch = useDispatch();

  const codeSortIcon =
    sort === 'code' ? (order === 'asc' ? ICON_TYPE.UP : ICON_TYPE.DOWN) : ICON_TYPE.SORT;
  const nameSortIcon =
    sort === 'name' ? (order === 'asc' ? ICON_TYPE.UP : ICON_TYPE.DOWN) : ICON_TYPE.SORT;

  const onCodeSort = () => {
    const sorting: adminSortType = 'code';
    const ordering: adminSortOrder = sort === 'name' || order === 'desc' ? 'asc' : 'desc';
    dispatch(sortAdminProduct(sorting, ordering));
  };

  const onNameSort = () => {
    const sorting: adminSortType = 'name';
    const ordering: adminSortOrder = sort === 'name' || order === 'desc' ? 'asc' : 'desc';
    dispatch(sortAdminProduct(sorting, ordering));
  };

  const setAllSelected = (value: boolean) => dispatch(selectAdminAllProducts(value));

  return (
    <div className="admin-header-row">
      <CheckboxCol>
        <Checkbox checked={isChecked} onChange={setAllSelected} disabled={!products} />
      </CheckboxCol>
      <CodeCol>
        <FormattedMessage id="admin.code" />
        <div onClick={onCodeSort}>
          <Icon icon={codeSortIcon} iconClassName={sort === 'code' ? '' : 'disabled'} />
        </div>
      </CodeCol>
      <NameCol>
        <FormattedMessage id="admin.name" />
        <div onClick={onNameSort}>
          <Icon icon={nameSortIcon} iconClassName={sort === 'name' ? '' : 'disabled'} />
        </div>
      </NameCol>
      <ActionCol>
        <FormattedMessage id="admin.actions" />
      </ActionCol>
    </div>
  );
};

interface Props {
  isChecked?: boolean;
  products?: number;
  sort?: adminSortType;
  order?: adminSortOrder;
}

const mapStateToProps = (state: StateInterface) => ({
  isChecked: areAllSelectedProducts(state),
  products: getTotalProductsCount(state),
  sort: getAdminSort(state),
  order: getAdminOrder(state),
});

export default connect(mapStateToProps)(AdminHeader);
