import { FC, ReactNode } from 'react';

export const CheckboxCol: FC<ICheckboxColProps> = ({ children, className }) => {
  return (
    <div className={`table-cell ${className ?? ''}`} style={{ width: '50px' }}>
      {children}
    </div>
  );
};

interface ICheckboxColProps {
  children?: ReactNode;
  className?: string;
}

export const CodeCol: FC<ICodeColProps> = ({ children, className }) => {
  return (
    <div className={`table-cell ${className ?? ''}`} style={{ flex: 2 }}>
      {children}
    </div>
  );
};

interface ICodeColProps {
  children: ReactNode;
  className?: string;
}

export const NameCol: FC<INameColProps> = ({ children, className }) => {
  return (
    <div className={`table-cell ${className ?? ''}`} style={{ flex: 2 }}>
      {children}
    </div>
  );
};

interface INameColProps {
  children: ReactNode;
  className?: string;
}

export const ActionCol: FC<IActionColProps> = ({ children, className }) => {
  return (
    <div className={`table-cell ${className ?? ''}`} style={{ flex: 1 }}>
      {children}
    </div>
  );
};

interface IActionColProps {
  children: ReactNode;
  className?: string;
}
