/* eslint-disable react/display-name */
import { FC, forwardRef, CSSProperties, ReactNode, Ref, HTMLAttributes } from 'react';
import { VirtuosoGrid as Grid, GridComponents, GridItem } from 'react-virtuoso';
import './VirtualGrid.css';

interface GridListProps {
  style?: CSSProperties;
  children?: ReactNode;
}

interface GridItemProps extends GridItem, HTMLAttributes<HTMLDivElement> {
  context?: unknown;
  children?: React.ReactNode;
}

const gridComponents: GridComponents<unknown> = {
  List: forwardRef<HTMLDivElement, GridListProps>(({ style, children, ...props }, ref) => (
    <div ref={ref as Ref<HTMLDivElement>} {...props} className="grid-list" style={{ ...style }}>
      {children}
    </div>
  )),
  Item: ({ children, ...props }: GridItemProps) => (
    <div {...props} className="grid-item">
      {children}
    </div>
  ),
};

const VitualGrid: FC<Props<unknown>> = (props) => {
  const {
    className = '',
    instances = [],
    style = {},
    EmptyComponent = () => <div></div>,
    RowComponent,
  } = props;

  const listStyle = { height: '100%', width: '100%' };

  return (
    <div className={`list ${className}`} style={style}>
      {instances.length > 0 ? (
        <Grid
          itemContent={RowComponent}
          totalCount={instances.length}
          style={listStyle}
          components={gridComponents}
        />
      ) : (
        <EmptyComponent />
      )}
    </div>
  );
};

interface Props<T> {
  className?: string;
  instances: T[];
  style?: object;
  EmptyComponent?: FC;
  RowComponent: (idx: number) => JSX.Element;
}

export default VitualGrid;
