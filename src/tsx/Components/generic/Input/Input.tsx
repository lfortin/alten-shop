import { ChangeEvent, FC } from 'react';
import { useIntl } from 'react-intl';
import './Input.css';

const Input: FC<Props> = ({
  type = 'text',
  className,
  disabled,
  placeholder,
  testId,
  translatePh = true,
  value = '',
  onChange,
}) => {
  const intl = useIntl();

  const placeholderLabel = translatePh ? intl.formatMessage({ id: placeholder }) : placeholder;

  const onChangeValue = (e: ChangeEvent<HTMLInputElement>) => {
    onChange(e?.target?.value);
  };

  return (
    <div className={`app-input-text ${className}`}>
      <div data-testid={testId} className={disabled ? 'disabled' : ''}>
        <input
          value={value}
          type={type}
          placeholder={placeholderLabel}
          onChange={onChangeValue}
          disabled={disabled}
        />
      </div>
    </div>
  );
};

interface Props {
  type?: string;
  className?: string;
  disabled?: boolean;
  placeholder: string;
  testId?: string;
  translatePh?: boolean;
  value?: string;
  onChange: (value: string) => void;
}

export default Input;
