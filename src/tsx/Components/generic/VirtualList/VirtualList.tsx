import { FC } from 'react';
import { Virtuoso as List } from 'react-virtuoso';
import './VirtualList.css';

const VirtualList: FC<Props<unknown>> = (props) => {
  const {
    className = '',
    instances = [],
    style = {},
    EmptyComponent = () => <div></div>,
    RowComponent,
  } = props;

  const listStyle = { height: '100%', width: '100%' };

  return (
    <div className={`list ${className}`} style={style}>
      {instances.length > 0 ? (
        <List itemContent={RowComponent} totalCount={instances.length} style={listStyle} />
      ) : (
        <EmptyComponent />
      )}
    </div>
  );
};

interface Props<T> {
  className?: string;
  instances: T[];
  style?: object;
  EmptyComponent?: FC;
  RowComponent: (idx: number) => JSX.Element;
}

export default VirtualList;
