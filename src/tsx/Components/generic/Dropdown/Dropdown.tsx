import { FC } from 'react';
import './Dropdown.css';

export type SelectedOption = string | number;

export interface DropdownOption {
  value: SelectedOption;
  label: string;
}

const Dropdown: FC<Props> = (props) => {
  const { className, options, selectedOption, height, width, onChange } = props;

  const propsStyle = {
    minWidth: width ? `${width}rem` : '10rem',
    minHeight: height ? `${height}rem` : '1.5rem',
  };

  const handleOptionChange = (e: React.ChangeEvent<HTMLSelectElement>): void => {
    const newOption = e.target.value;
    onChange(newOption);
  };

  return (
    <select
      value={selectedOption}
      onChange={handleOptionChange}
      className={`select ${className}`}
      style={propsStyle}
    >
      {options.map(({ value, label }) => (
        <option key={value} value={value}>
          {label}
        </option>
      ))}
    </select>
  );
};

interface Props {
  className?: string;
  options: DropdownOption[];
  selectedOption?: SelectedOption;
  height?: number;
  width?: number;
  onChange: (selectedOption: SelectedOption) => void;
}

export default Dropdown;
