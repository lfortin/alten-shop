import { FC } from 'react';
import { Location } from 'react-router-dom';
import './Header.css';
// redux
// components
import ThemeSelector from '../ThemeSelector';
import Icon from '../Icon';
import { ICON_TYPE } from '../Icon/Icon';
// libs

const Header: FC<Props> = () => {
  return (
    <div className="header">
      <ThemeSelector />
      <span className="username ml20 mr20">John Doe</span>
      <Icon iconClassName="mr20 user-icon" icon={ICON_TYPE.USER} />
    </div>
  );
};

interface Props {
  location?: Location;
}

export default Header;
