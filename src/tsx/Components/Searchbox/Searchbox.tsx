/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect, useState } from 'react';
import { useDebounceValue } from 'usehooks-ts';
import './Searchbox.css';
import Icon from '../Icon';
import { ICON_TYPE } from '../Icon/Icon';
import Input from '../generic/Input';

const Searchbox: FC<Props> = ({ debounced = false, placeholder, testId, text, onSearch }) => {
  const [debouncedValue, setDebouncedValue] = useDebounceValue(text, 500);
  const [liveValue, setValue] = useState(text);

  useEffect(() => {
    if (debounced) onSearch(debouncedValue);
  }, [debouncedValue]);

  const onSearchFunc = (value: string): void => {
    setValue(value);
    if (debounced) setDebouncedValue(value);
    else onSearch(value);
  };

  const clearInput = () => onSearchFunc('');

  const isEmpty = !liveValue;
  return (
    <div className="text-input">
      <Input
        onChange={onSearchFunc}
        className="input"
        type="text"
        placeholder={placeholder ?? 'search.default'}
        testId={testId}
        value={liveValue}
      />

      <div onClick={clearInput} className={`clear-input ${isEmpty ? 'disabled' : ''}`}>
        <Icon icon={isEmpty ? ICON_TYPE.SEARCH : ICON_TYPE.CANCEL} iconClassName="flex" />
      </div>
    </div>
  );
};

interface Props {
  debounced?: boolean;
  placeholder?: string;
  testId?: string;
  text: string;
  onSearch: (text: string) => void;
}

export default Searchbox;
