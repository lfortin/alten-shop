import { FC } from 'react';
import Ic from '@mdi/react';
import './Icon.css';

// import from https://pictogrammers.com/library/mdi/
import {
  mdiAccountOutline,
  mdiAccountMultipleOutline,
  mdiArrowUp,
  mdiArrowDown,
  mdiCartOutline,
  mdiCheckBold,
  mdiClose,
  mdiDeleteOutline,
  mdiMagnify,
  mdiMenu,
  mdiMenuLeft,
  mdiPencilOutline,
  mdiPlus,
  mdiTagOutline,
  mdiViewGridOutline,
  mdiWeatherNight,
  mdiWhiteBalanceSunny,
  mdiArrowUpDown,
} from '@mdi/js';

export const ICON_TYPE = {
  ADD: mdiPlus,
  ADMIN: mdiAccountMultipleOutline,
  CANCEL: mdiClose,
  CHECKED: mdiCheckBold,
  DARK_MODE: mdiWeatherNight,
  DELETE: mdiDeleteOutline,
  DOWN: mdiArrowDown,
  EDIT: mdiPencilOutline,
  FOLD_MENU: mdiMenuLeft,
  LIGHT_MODE: mdiWhiteBalanceSunny,
  MENU: mdiMenu,
  PRODUCTS: mdiCartOutline,
  SEARCH: mdiMagnify,
  SORT: mdiArrowUpDown,
  TAG: mdiTagOutline,
  TILE: mdiViewGridOutline,
  UP: mdiArrowUp,
  USER: mdiAccountOutline,
};

const DEFAUT_SIZE = 24;

export type IconType = (typeof ICON_TYPE)[keyof typeof ICON_TYPE];

const Icon: FC<Props> = ({ icon, iconClassName, size = DEFAUT_SIZE, color, title }) => {
  return (
    <div className={`app-icon ${iconClassName}`}>
      <Ic
        path={icon}
        title={title}
        color={color}
        style={{ height: `${size}px`, width: `${size}px` }}
      />
    </div>
  );
};

interface Props {
  icon: IconType;
  iconClassName?: string;
  size?: number;
  color?: string;
  title?: string;
}

export default Icon;
