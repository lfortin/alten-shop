/* eslint-disable react-hooks/exhaustive-deps */
import { FC, memo, useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import './Admin.css';
// redux
import { setTitle } from '../../../actions/application';
import { addProduct, fetchProducts } from '../../../api/products';
import { StateInterface } from '../../../reducers';
import { selectAdminProducts } from '../../../selectors/products';
import { Product } from '../../../models/products';
// components
import AdminActions from '../../Components/Admin/AdminActions';
import AdminHeader from '../../Components/Admin/AdminHeader';
import AdminListItem from '../../Components/Admin/AdminListItem';
import AdminAddItem from '../../Components/Admin/AdminAddItem';
import VirtualList from '../../Components/generic/VirtualList';
import Spinner from '../../Components/Spinner';

const ListItem: FC<ListItemProps> = ({ instance }) => (
  <div className="item flex justify-center ">
    <AdminListItem product={instance} />
  </div>
);

interface ListItemProps {
  instance: Product;
  key: number | string;
  _idx: number;
}

const EmptyList: FC = () => (
  <div className="h100 w100 flex-cc">
    <FormattedMessage id="products.empty" />
  </div>
);

const Admin: FC<Props> = ({ products }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [isAdding, setAdding] = useState(false);

  useEffect(() => {
    dispatch(setTitle('app.title'));
    setLoading(true);
    fetchProducts()
      .then(() => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  if (loading) {
    return (
      <div className="h100 w100 flex-cc">
        <Spinner size={3} />
      </div>
    );
  }

  const onAddItem = (code: string, name: string) => {
    void addProduct(code, name)
      .then(() => setAdding(false))
      .catch(() => setAdding(false));
  };

  const RowComponent = (idx: number) => (
    <ListItem key={products[idx].id} instance={products[idx]} _idx={idx} />
  );

  return (
    <div className="admin-container flex-col grow">
      <div className="admin-header">
        <FormattedMessage id="admin.header" />
      </div>
      <AdminActions onAdd={() => setAdding(true)} isAdding={isAdding} />
      <AdminHeader />
      {isAdding && <AdminAddItem onAdd={onAddItem} onCancel={() => setAdding(false)} />}
      <div className="admin-list">
        <VirtualList
          className="mb50"
          instances={products}
          RowComponent={RowComponent}
          EmptyComponent={() => <EmptyList />}
        />
      </div>
    </div>
  );
};

interface Props {
  products: Product[];
}

const mapStateToProps = (state: StateInterface): Props => ({
  products: selectAdminProducts(state),
});

/**
 * @param {*} props
 * @param {*} nextProps
 * @returns true if component does not need to re-render
 */
const equalProps = (props: Props, nextProps: Props) =>
  JSON.stringify(props.products.map((p) => p.id)) ===
    JSON.stringify(nextProps.products.map((p) => p.id)) &&
  JSON.stringify(props.products.map((p) => p.name)) ===
    JSON.stringify(nextProps.products.map((p) => p.name));

export default connect(mapStateToProps)(memo(Admin, equalProps));
