/* eslint-disable react-hooks/exhaustive-deps */
import { FC, memo, useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import './Products.css';
// redux
import { setTitle } from '../../../actions/application';
import { setProductFilter, setProductSort, sortType } from '../../../actions/products';
import { fetchProducts } from '../../../api/products';
import { StateInterface } from '../../../reducers';
import {
  getFilters,
  getSort,
  getTotalProductsCount,
  selectProducts,
} from '../../../selectors/products';
import { Product, ProductFilters } from '../../../models/products';
// components
import ProductItem from '../../Components/Products/ProductItem';
import Button from '../../Components/generic/Button';
import Dropdown, { DropdownOption, SelectedOption } from '../../Components/generic/Dropdown';
import VirtualList from '../../Components/generic/VirtualList';
import VirtualGrid from '../../Components/generic/VirtualGrid';
import Spinner from '../../Components/Spinner';
import Searchbox from '../../Components/Searchbox';
import Paginator from '../../Components/Paginator';
import Icon from '../../Components/Icon';
import { ICON_TYPE } from '../../Components/Icon/Icon';

const TileComponent = 'TileComponent';
const ListComponent = 'ListComponent';

type ItemComponentType = typeof TileComponent | typeof ListComponent;

const ListItem: FC<ListItemProps> = ({ componentClassName, instance }) => (
  <div className="item flex justify-center ">
    <ProductItem product={instance} className={componentClassName} />
  </div>
);

interface ListItemProps {
  instance: Product;
  componentClassName: string;
  key: number | string;
  _idx: number;
}

const EmptyList: FC<EmptyListProps> = ({ search }) => (
  <div className="h100 w100 flex-cc">
    <FormattedMessage id={search ? 'products.empty.search' : 'products.empty'} />
  </div>
);

interface EmptyListProps {
  search: boolean;
}

const Products: FC<ProductsProps> = ({ count, filters, products, sortField }) => {
  const dispatch = useDispatch();
  const intl = useIntl();
  const [component, setComponent] = useState(TileComponent as ItemComponentType);
  const [loading, setLoading] = useState(false);
  const [filterString, setFilter] = useState('');

  useEffect(() => {
    dispatch(setTitle('app.title'));
    setLoading(true);
    fetchProducts()
      .then(() => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  if (loading) {
    return (
      <div className="h100 w100 flex-cc">
        <Spinner size={3} />
      </div>
    );
  }

  const onSort = (field: SelectedOption) => dispatch(setProductSort(field as sortType));

  const onSearch = (value: string): void => {
    setFilter(value);
    dispatch(setProductFilter('search', value));
  };

  const onSwitchView = (view: ItemComponentType) => setComponent(view);

  const sortOptions: DropdownOption[] = [
    { label: intl.formatMessage({ id: 'product.sort.name' }), value: 'name' },
    { label: intl.formatMessage({ id: 'product.sort.price' }), value: 'price' },
    { label: intl.formatMessage({ id: 'product.sort.category' }), value: 'category' },
    { label: intl.formatMessage({ id: 'product.sort.rating' }), value: 'rating' },
  ];

  const componentClassName = component === TileComponent ? 'product-tile' : 'product-row';
  const List = component === TileComponent ? VirtualGrid : VirtualList;

  const RowComponent = (idx: number) => (
    <ListItem
      key={products[idx].id}
      instance={products[idx]}
      componentClassName={componentClassName}
      _idx={idx}
    />
  );

  return (
    <div className="products-container flex-col grow">
      <div className="products-header">
        <FormattedMessage id="products.header" />
      </div>
      <div className="list-layout">
        <div className="parameters">
          <div className="filters">
            <Dropdown options={sortOptions} onChange={onSort} selectedOption={sortField} />
            <Searchbox
              onSearch={onSearch}
              placeholder="search.products.placeholder"
              testId="products-searchbox"
              text={filterString}
              debounced
            />
          </div>
          <div className="list-component-switch">
            <Button
              className={component === TileComponent ? 'reverse' : ''}
              onClick={() => onSwitchView(ListComponent)}
            >
              <Icon icon={ICON_TYPE.MENU} size={16} />
            </Button>
            <Button
              className={component === ListComponent ? 'reverse' : ''}
              onClick={() => onSwitchView(TileComponent)}
            >
              <Icon icon={ICON_TYPE.TILE} size={16} />
            </Button>
          </div>
        </div>
        <List
          className="mb20"
          instances={products}
          RowComponent={RowComponent}
          EmptyComponent={() => <EmptyList search={!!filterString} />}
        />
      </div>
      <Paginator filters={filters} count={count} />
    </div>
  );
};

interface ProductsProps {
  products: Product[];
  filters: ProductFilters;
  count: number;
  sortField: sortType;
}

const mapStateToProps = (state: StateInterface): ProductsProps => ({
  count: getTotalProductsCount(state),
  filters: getFilters(state),
  products: selectProducts(state),
  sortField: getSort(state),
});

/**
 * @param {*} props
 * @param {*} nextProps
 * @returns true if component does not need to re-render
 */
const equalProps = (props: ProductsProps, nextProps: ProductsProps) =>
  JSON.stringify(props.products.map((p) => p.id)) ===
    JSON.stringify(nextProps.products.map((p) => p.id)) &&
  JSON.stringify(props.filters) === JSON.stringify(nextProps.filters) &&
  props.count === nextProps.count &&
  props.sortField === nextProps.sortField;

export default connect(mapStateToProps)(memo(Products, equalProps));
