import { Page, Locator } from '@playwright/test';

export const screenshot = async (page: Page, name: string) =>
  await page.screenshot({ path: `playwright-tests/screenshots/${name}.png`, fullPage: true });

export const elementScreenshot = async (element: Locator, name: string) =>
  await element.screenshot({ path: `playwright-tests/screenshots/${name}.png` });
