import { test, expect } from '@playwright/test';
import { hexToRgb } from '../../utils/colors';

test.beforeEach(async ({ page }) => {
  await page.goto('/');
});

test.describe('Layout', () => {
  test('should be displayed', async ({ page }) => {
    const header = page.locator('.app-content').locator('.header');
    await expect(header).toBeVisible();
  });

  test('should contain user name', async ({ page }) => {
    const header = page.locator('.app-content').locator('.header');
    const userName = header.locator('.username');
    await expect(userName).toBeVisible();
  });

  test('should contain user icon', async ({ page }) => {
    const header = page.locator('.app-content').locator('.header');
    const userIcon = header.locator('.user-icon');
    await expect(userIcon).toBeVisible();
  });
});

test.describe('Theme', () => {
  test('should contain the theme selector', async ({ page }) => {
    const header = page.locator('.app-content').locator('.header');
    const themeSelector = header.locator('.theme-selector');
    await expect(themeSelector).toBeVisible();
  });

  test('should be able to change the theme', async ({ page }) => {
    const lightBG = '#f5f5f5';
    const lightColor = '#232a2f';
    const darkBG = '#232a2f';
    const darkColor = '#ffffff';

    const app = page.locator('.app-wrapper');
    const themeSelector = page
      .locator('.app-content')
      .locator('.header')
      .locator('.theme-selector');

    await expect(app).toHaveCSS('background-color', hexToRgb(lightBG));
    await expect(app).toHaveCSS('color', hexToRgb(lightColor));

    await themeSelector.click();
    await expect(app).toHaveCSS('background-color', hexToRgb(darkBG));
    await expect(app).toHaveCSS('color', hexToRgb(darkColor));
  });
});
