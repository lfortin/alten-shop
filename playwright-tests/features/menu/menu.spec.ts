import { test, expect } from '@playwright/test';
import { EN, FR, LocaleType, intl } from '../../../src/libs/i18n/initReactIntl';
import en from '../../../src/libs/i18n/en.json' assert { type: 'json' };
import fr from '../../../src/libs/i18n/fr.json' assert { type: 'json' };

test.beforeEach(async ({ page }) => {
  await page.goto('/');
});

test.describe('Layout', () => {
  test('should be displayed', async ({ page }) => {
    const menu = page.locator('.menu');
    await expect(menu).toBeVisible();
  });

  test('should collapse and open', async ({ page }) => {
    const menu = page.locator('.menu');
    const collapseIcon = menu.locator('.title').locator('.icon').locator('.app-icon');

    await expect(collapseIcon).toBeVisible();
    await expect(menu).toHaveClass(/open/);

    await collapseIcon.click();
    const burgerIcon = menu.locator('.title').locator('.icon').locator('.app-icon');
    await expect(burgerIcon).toBeVisible();
    await expect(menu).not.toHaveClass(/open/);

    await burgerIcon.click();
    await expect(menu).toHaveClass(/open/);
  });

  test('should contain links', async ({ page }) => {
    const menu = page.locator('.menu');
    const links = menu.locator('.menu-links');
    await expect(links).toBeVisible();
    await expect(links).toHaveClass(/grow/);
  });

  test('should contain footer', async ({ page }) => {
    const menu = page.locator('.menu');
    const footer = menu.locator('.menu-bottom');
    await expect(footer).toBeVisible();
  });
});

test.describe('Navigation', () => {
  test('Admin item should be default active', async ({ page }) => {
    const menu = page.locator('.menu');
    const adminLink = menu.getByTestId('to-admin');
    await expect(adminLink).toHaveClass(/active/);
  });

  test('Navigate to products view', async ({ page }) => {
    const menu = page.locator('.menu');
    const adminLink = menu.getByTestId('to-admin');
    const targetLink = menu.getByTestId('to-products');
    await expect(targetLink).not.toHaveClass(/active/);
    await expect(page).toHaveURL(/admin\/products/);

    await targetLink.click();
    const pageContent = page.locator('.app-content');

    await expect(adminLink).not.toHaveClass(/active/);
    await expect(targetLink).toHaveClass(/active/);
    await expect(page).toHaveURL(/products/);
    expect(pageContent).toBeDefined();
    await expect(pageContent).toBeVisible();
  });
});

test.describe('Footer', () => {
  test('should contain the locale selector', async ({ page }) => {
    const footer = page.locator('.menu').locator('.menu-bottom');
    const localeSelector = footer.locator('.locale-selector');
    await expect(localeSelector).toHaveClass(/select/);
  });

  test('should be able to change the locale', async ({ page }) => {
    const linkTranslation = (locale: LocaleType): string =>
      locale === EN ? en['pages.admin'] : fr['pages.admin'];

    const adminLink = page.locator('.menu').getByTestId('to-admin');
    const localeSelector = page
      .locator('.menu')
      .locator('.menu-bottom')
      .locator('.locale-selector');
    const targetLanguage = localeSelector.locator('option', {
      hasText: intl.formatMessage({ id: 'locale.fr' }),
    });

    await expect(adminLink).toHaveText(linkTranslation(EN));
    await expect(targetLanguage).toBeHidden();

    await localeSelector.selectOption({ value: FR });
    await expect(adminLink).toHaveText(linkTranslation(FR));
  });
});
