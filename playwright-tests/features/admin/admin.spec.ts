import { test, expect } from '@playwright/test';
import { intl } from '../../../src/libs/i18n/initReactIntl';

test.beforeEach(async ({ page }) => {
  await page.goto('/admin/products');
});

test.describe('App title', () => {
  test('Should be defined', async ({ page }) => {
    const pageTitle = intl.formatMessage({ id: 'app.title' });
    await expect(page).toHaveTitle(pageTitle);
  });
});

test.describe('loader', () => {
  test('should be displayed', async ({ page }) => {
    const spinner = page.locator('.spinner');
    await expect(spinner).toBeVisible();
  });

  test('should be displayed and then disapear', async ({ page }) => {
    const spinner = page.locator('.spinner');
    await expect(spinner).toBeVisible();
    await expect(spinner).toBeHidden();
  });
});

test.describe('List', () => {
  test('should be displayed', async ({ page }) => {
    const list = page.locator('.list');
    await expect(list).toBeVisible();
  });

  test('should contain 30 elements', async ({ page }) => {
    const listSelector = '.list';
    const list = page.locator(listSelector);
    const items = await list.locator('.item').count();
    expect(items).toBeLessThanOrEqual(30);
  });
});
