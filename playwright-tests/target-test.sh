#!/bin/bash

if [ -d "playwright-tests/features/$1" ]; then
  # Execute tests
  playwright test playwright-tests/features/$1
else
  # Target folder not found
  echo " "
  echo " "
  echo "                            ERROR"
  echo "Could not find the targeted folder 'playwright-tests/features/$1'."
  echo "Please execute the 'yarn p-test' command if you want to execute all tests."
  echo " "
  echo " "
fi
